import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import static java.util.FormatProcessor.FMT;

public class Main {

  interface ResultComputer {
    List<List<Integer>> compute(int length, int[] words);
  }

  enum Computers implements ResultComputer {
    SEQUENTIAL(Main::findWordsNumericSequential),
    THREAD_POOL(Main::findWordsNumericThreadPool),
    VIRTUAL_THREADS(Main::findWordsNumericParallel);

    private final ResultComputer resultComputer;

    Computers(ResultComputer resultComputer) {
      this.resultComputer = resultComputer;
    }

    public ResultComputer getResultComputer() {
      return resultComputer;
    }

    @Override
    public List<List<Integer>> compute(int length, int[] words) {
      return resultComputer.compute(length, words);
    }
  }

  public static List<String> loadNLetterWords(Path source, int n) throws IOException {
    try (var reader = Files.newBufferedReader(source)) {
      return reader.lines().filter(word -> word.length() == n).toList();
    }
  }

  public static int encodeBitSet(String word) {
    return word.chars().map(i -> i - 'a').reduce(0, (a, b) -> a | 1 << b);
  }

  public static Map<Integer, List<String>> createIndex(List<String> words) {
    var result = new TreeMap<Integer, List<String>>();

    for (var word : words) {
      var bitset = encodeBitSet(word);
      if (Integer.bitCount(bitset) != word.length()) {
        continue;
      }

      result.computeIfAbsent(bitset, _ -> new ArrayList<>()).add(word);
    }

    return result;
  }

  public static int[] prune(int[] words, int word) {
    return IntStream.of(words).filter(key -> key > word && (key & word) == 0).toArray();
  }

  public static List<List<Integer>> findWordsNumericParallel(int length, int[] words) {
    if (length == 0) {
      return List.of(List.of());
    }

    List<List<Integer>> results = Collections.synchronizedList(new ArrayList<>());

    List<Thread> threads = new ArrayList<>();

    for (int word : words) {
      threads.add(Thread.ofVirtual().start(() -> {
        List<Integer> nextSequence = new ArrayList<>();
        nextSequence.add(word);
        int[] nextWords = prune(words, word);

        if (length > 1 && nextWords.length == 0) {
          return;
        }

        findWordsNumeric(length - 1, nextSequence, nextWords, results);
      }));
    }

    System.out.println(FMT. "started \{ threads.size() } threads" );

    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        throw new RuntimeException(e);
      }
    }

    return results;
  }

  public static List<List<Integer>> findWordsNumericThreadPool(int length, int[] words) {
    if (length == 0) {
      return List.of(List.of());
    }

    List<List<Integer>> results = Collections.synchronizedList(new ArrayList<>());

    try(var executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors())) {

      List<Future<?>> futures = new ArrayList<>();

      for (int word : words) {
        futures.add(executor.submit(() -> {
          List<Integer> nextSequence = new ArrayList<>();
          nextSequence.add(word);
          int[] nextWords = prune(words, word);

          if (length > 1 && nextWords.length == 0) {
            return;
          }

          findWordsNumeric(length - 1, nextSequence, nextWords, results);
        }));
      }

      System.out.println(FMT. "started \{ futures.size() } runnables" );

      for (Future<?> future : futures) {
        try {
          future.get();
        } catch (InterruptedException | ExecutionException e) {
          throw new RuntimeException(e);
        }
      }
    }

    return results;
  }

  public static List<List<Integer>> findWordsNumericSequential(int length, int[] words) {
    if (length == 0) {
      return List.of(List.of());
    }

    List<List<Integer>> results = new ArrayList<>();

    findWordsNumeric(length, new ArrayList<>(), words, results);

    return results;
  }

  public static void findWordsNumeric(int length, List<Integer> currentSequence, int[] words,
      List<List<Integer>> results) {
    if (length == 0) {
      results.add(new ArrayList<>(currentSequence));
      return;
    }

    for (int word : words) {
      int[] nextWords = prune(words, word);
      if (length > 1 && nextWords.length == 0) {
        continue;
      }
      currentSequence.add(word);
      findWordsNumeric(length - 1, currentSequence, nextWords, results);
      currentSequence.removeLast();
    }
  }


  public static List<List<String>> resolvePermutations(Map<Integer, List<String>> index, List<Integer> words) {
    List<List<String>> result = new ArrayList<>();
    resolvePermutations(index, words, new ArrayList<>(), result);
    return result;
  }

  public static void resolvePermutations(Map<Integer, List<String>> index, List<Integer> words,
      List<String> stringWords, List<List<String>> result) {
    if (words.isEmpty()) {
      result.add(new ArrayList<>(stringWords));
      return;
    }

    var first = words.getFirst();
    var tail = words.subList(1, words.size());
    for (var word : index.get(first)) {
      stringWords.add(word);
      resolvePermutations(index, tail, stringWords, result);
      stringWords.removeLast();
    }
  }

  private static List<List<String>> computeResults(Computers resultComputer) throws IOException {
    Instant tStart = Instant.now();
    var words = loadNLetterWords(Paths.get("res", "words_alpha.txt"), 5);
    System.out.println(FMT. "loaded \{ words.size() } five letter words" );
    Instant tReadWords = Instant.now();

    var index = createIndex(words);
    System.out.println(FMT. "\{ index.size() } five letter words without permutations" );
    int[] wordsNumbers = index.keySet().stream().mapToInt(Integer::valueOf).sorted().toArray();

    Instant tCreateIndex = Instant.now();

    var resultsKeys = resultComputer.compute(5, wordsNumbers);
    System.out.println(FMT. "solutions without permutations: \{ resultsKeys.size() } using \{ resultComputer.name() }" );
    Instant tFindWithoutPerm = Instant.now();

    List<List<String>> solutions = resultsKeys.stream()
        .flatMap(wordTuple -> resolvePermutations(index, wordTuple).stream()).toList();
    System.out.println(FMT. "solutions with permutations: \{ solutions.size() }" );

    Instant tFindWithPerms = Instant.now();

    System.out.println(FMT. "load file: \{ Duration.between(tStart, tReadWords).toMillis() }ms" );
    System.out.println(FMT. "create index: \{ Duration.between(tReadWords, tCreateIndex).toMillis() }ms" );
    System.out.println(FMT. "find without perm: \{ Duration.between(tCreateIndex, tFindWithoutPerm).toMillis() }ms" );
    System.out.println(FMT. "find with perm: \{ Duration.between(tFindWithoutPerm, tFindWithPerms).toMillis() }ms" );
    System.out.println(FMT. "end to end: \{ Duration.between(tStart, tFindWithPerms).toMillis() }ms" );

    return solutions;
  }

  public static void main(String[] args) throws Exception {

    var resultComputer = Computers.valueOf(System.getProperty("result.computer", Computers.VIRTUAL_THREADS.name()).toUpperCase(
        Locale.ROOT));

    System.out.println(FMT. "configured computer: \{ resultComputer.name() }");

    var warmups = Integer.parseInt(System.getProperty("warmups", "0"));

    for (int i = 0; i < warmups; i++) {
      System.out.println(FMT. "Warmup \{i}");
      computeResults(resultComputer);
    }

    List<List<String>> solutions = computeResults(resultComputer);

    if (System.getProperty("output.file") != null) {
      try (var writer = Files.newBufferedWriter(Paths.get(System.getProperty("output.file")))) {
        for (List<String> solution : solutions) {
          writer.write(FMT. "\{ String.join(",", solution) }\n" );
        }
      }
    }
  }

}